import React from 'react';
import styles from './Header.module.css';

import thomasLogo from '../../assets/images/thomas-logo.svg';
import kddLogo from '../../assets/images/kdd-logo.svg';
import salesCircle from '../../assets/images/sales-circle.png';
import card from '../../assets/images/card.png';
import cards from '../../assets/images/cards.png';
import cardMobile from '../../assets/images/card-mobile.png';

const WIDTH = window.innerWidth;

const Header = () => {
	return (
		<header className={styles.header}>
			<div className='container'>
				<div className={styles.wrap}>
					<div className={styles.row + ' ' + styles.rowHeader}>
						<div className={styles.thomas}>
							<img src={thomasLogo} alt='Thomas logo' />
						</div>
						<div className={styles.dixy}>
							{WIDTH > 500 ? (
								<img src={cards} alt='Kdd logo' />
							) : (
								<img src={kddLogo} alt='Kdd logo' />
							)}
						</div>
					</div>

					<div className={styles.signature}>
						<h1>ЭКСКЛЮЗИВНЫЕ СТОЛОВЫЕ ПРИБОРЫ</h1>
					</div>

					<div className={styles.sale}>
						<div className={styles.saleCircle}>
							<img src={salesCircle} alt='Sale' />
						</div>
						<p>
							Получайте скидки по карте «Клуба Друзей ДИКСИ» или
							собирайте наклейки
						</p>

						{WIDTH <= 500 ? (
							<div className={styles.cards}>
								<img src={cardMobile} alt='Card' />
								<img src={cardMobile} alt='Card' />
								<img src={cardMobile} alt='Card' />
							</div>
						) : (
							<img src={kddLogo} alt='Kdd logo' />
						)}
					</div>
					<p className={styles.title}>СОБИРАЙТЕ СЕМЬЮ ЗА СТОЛОМ!</p>
					<p className={styles.subtitle}>с 19 июля по 31 октября</p>
				</div>
			</div>
		</header>
	);
};

export default Header;
