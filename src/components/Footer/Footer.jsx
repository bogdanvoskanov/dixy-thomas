import React from 'react';
import styles from './Footer.module.css'

import leaflet from '../../assets/leaflet.pdf'

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <p className={styles.agree}>
        Внимание! Принимая участие в акции, участник подтверждает свое согласие
        с <a href={leaflet}>настоящими&nbsp;правилами</a> и со всеми условиями акции. Количество товара
        ограничено. Внешний вид и характеристики товаров могут отличаться от
        размещенных в буклетах.
      </p>
      <p>
        Информацию об организаторах акции, правилах ее проведения, количестве
        товаров по специальной цене, сроках, месте и порядке их приобретения вы
        можете получить по телефону бесплатной горячей линии <a href="tel:+78003330201">8&nbsp;800&nbsp;333&nbsp;0201</a>, а
        также на сайте <a href="https://dixy.ru">dixy.ru</a>.
      </p>
    </footer>
  );
};

export default Footer;
